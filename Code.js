/**
 * Created by revilofr for all the No Man's Sky fans.
 * https://github.com/revilofr
 * Feel free to adapt it but mention the original version and author.
 * 
 * Versions :
 * - 0.5 : revilofr > Starting adding : already crafted items
 * - 0.4 : revilofr > 2022.04.28 Adding recipe sumups
 * - 0.3 : revilofr > 2022.04.19 Fixing recipes
 * - 0.2 : revilofr > 2022.04.18 Fixing build orders
 * - 0.1 : revilofr > 2022.04.17 first version
 */

const GATHERED = 'gathered';
const CRAFTED = 'crafted';
const ITEMS_DATARANGE = 'B3:L100';
const LOG_CELL = 'A7'; // where logs are to be written back onto the google spreadsheet
const ITEM_SHEET = 'items';
const COMPUTE_SHEET = 'compute';
const SEARCHED_ITEM_CELL = 'B4';
const SEARCHED_QTY_CELL = 'C4';
const RESULT_START_LINE = 11
const RESULT_GATH_START_COL="B";
const RESULT_CRAFT_START_COL="G";
const RESULT_END_LINE=51;

const SPREADSHEET = SpreadsheetApp.getActive();

// Column data indexes 
const INDEX_ITEM0_NAME=0;
const INDEX_TYPE=1;
const REQ_ITEM_OFFSET = 2;
const INDEX_REQ_ITM_QTY=0; // qty is the first culumn
const INDEX_REQ_ITM_IMG=1; // unused by script
const INDEX_REQ_ITM_NAME=2; // item name is the third one
const NEXT_REQ_ITM_OFFSET=3; // Number of column describin a required item (ie : 3 qty, icon, req. item)
const MAX_REQ_ITM_SIZE=3*NEXT_REQ_ITM_OFFSET // 10 as index means that the maximum of object by recipe can be 2 objects of 3 columns)

// Result
const CRAFT_STRATEGY_APPENDER= {}; // item to be craft, quatity
const GATHERABLE_APPENDER= {}; // Gatherable item quantities to provide to be able to craft the whole plan
let GATHERED_ARRAY=undefined;
let CRAFTED_ARRAY=undefined;

// Log
let _logAppender = '';

function refreshItems() {
  return SpreadsheetApp.getActive().getRange(ITEM_SHEET + "!"+ ITEMS_DATARANGE).getValues();
}

let ITEMS = refreshItems();

/**
 * Refresh in memory loaded items
 */


/**
 * Append a string or a json to the logs
 */
function _log(stringData, isJson=false){
  if (isJson){
    _logAppender+= JSON.stringify(stringData)+ "\n";
  }
  else{
    _logAppender+= stringData + "\n";
  }
}

/**
 * Clear the log
 */
function _clr(){
  _logAppender=""; 
}

/**
 * write previously appended logs into a debug cell output into the Spreadsheet
 */
function _writeLog(){
  SpreadsheetApp.getActive().getRange(COMPUTE_SHEET + "!" + LOG_CELL).setValue(_logAppender);
}


/**
 * Return the list of Gatherable items
 */
function getGatherableItems() {
  const items = ITEMS
    .filter(item=> item[1]===GATHERED)
    .map(val=> {
      return val.slice(0, -10); // skipping other column that name
      }
    );

  /**
  _clr();
  _log(items, true);
  _writeLog();
  */

  return items;
}

let GATHERED_ITEMS = getGatherableItems();

/**
 * Return the searched item with its recepies eventualy
 */
function _findItem(itemName){
  return ITEMS.filter(item => item[0]===itemName);
}

/**
 * Return true if the item is a Gatherable one
 */
function _isGatherable(itemName) {
  return GATHERED_ITEMS.filter(item=> item[0]===itemName).length > 0;
}

function _addToTableAppender(tableAppender, itemName, qty, parentName){
  if (tableAppender[itemName]){
    tableAppender[itemName]["qty"] += qty; //qty
    if (parentName +"".trim()!=="")
      tableAppender[itemName]["usedAsIngredientFor"].push(parentName);
  }
  else{
    const usedAsIngredientFor=(parentName +"".trim()!=="")?[parentName]:[];
    tableAppender[itemName] = {
      qty:qty,
      usedAsIngredientFor:usedAsIngredientFor
      };
  }
  return tableAppender;
}

function _extractNameFromCurrentItem(currentItem){
  return currentItem[0][INDEX_ITEM0_NAME];
}

/**
 * Get the list of req item
 */
function _getReqItem(craftItem){
  const reqItem = [];
  for (i = REQ_ITEM_OFFSET; i < MAX_REQ_ITM_SIZE; i += NEXT_REQ_ITM_OFFSET) { 
    const name = craftItem[0][i+INDEX_REQ_ITM_NAME];
    const qty = craftItem[0][i+INDEX_REQ_ITM_QTY];
    if (name.trim()!=="" && qty+"".trim()!=="" && qty>0 ){ //removing empty req items
      reqItem.push([name, qty]);
    }
  }
  return reqItem;
}

function _getSearchItemName(){
  return SPREADSHEET.getRange(COMPUTE_SHEET + "!"+ SEARCHED_ITEM_CELL).getValue();
}

function _getSearchItemQty(){
  return SPREADSHEET.getRange(COMPUTE_SHEET + "!"+ SEARCHED_QTY_CELL).getValue();
}

function _getMaterials(itemName='', reqQty=-1, sourceItemName='') {
  let searchedItem = itemName;
  let searchedQty = reqQty;
  if (!searchedItem){
    searchedItem=_getSearchItemName();
  }
  if (searchedQty===-1){
    searchedQty=_getSearchItemQty();
    searchedQty=parseInt(searchedQty);
  }

  const currentItem = _findItem(searchedItem);
  
  // If the item is a gatherable,
  if (_isGatherable(searchedItem)){
    // add item and quantity to the GATHERABLE_APPENDER
    //TODO
    _addToTableAppender(GATHERABLE_APPENDER,searchedItem,searchedQty, sourceItemName);
  }
  else {
    //_log ("The item is Craftable !");
    // if the item is a craftable,
    //   Add the item to be crafted to the CRAFT_STRATEGY_APPENDER
      _addToTableAppender(CRAFT_STRATEGY_APPENDER, searchedItem, searchedQty, sourceItemName);

    // For each req items
    const requestedItems = _getReqItem(currentItem);
    requestedItems.forEach(reqItem=>{
      //   Recursively call this method again with the item to be crafted as itemName
      const reqItemQty = reqItem[1]*searchedQty; // The req item qty is multiply by the source item qty
      const reqItemName = reqItem[0];
      const currentItemName = _extractNameFromCurrentItem(currentItem);
      _getMaterials(reqItemName,reqItemQty, currentItemName);
    })
  }
}

/** from json object to ready to import into spreadsheet */
function _objectToArray(objects){
  result = [];
  for (const [key, value] of Object.entries(objects)) {
    const name = key;
    const qty = value.qty;
    result.push([name, qty]);
  }

  return result;
}

function _writeToCells(gatherableResult, craftableResult){
  //Display GATHERABLES
  /**
   * 
   * const RESULT_GATH_START_COL="B";
   * const RESULT_CRAFT_START_COL="F";
   */
  SPREADSHEET.getRange(`B${RESULT_START_LINE}:C${RESULT_START_LINE+gatherableResult.length-1}`).setValues(gatherableResult);

  //Display CRAFTABLES
  SPREADSHEET.getRange(`G${RESULT_START_LINE}:I${RESULT_START_LINE+craftableResult.length-1}`).setValues(craftableResult);

}

function _cleanResult(){
  // clean GATHERABLE Results
  SPREADSHEET.getRange(`B${RESULT_START_LINE}:C${RESULT_END_LINE}`).clearContent();

  // clean CRAFTABLE Results
  SPREADSHEET.getRange(`G${RESULT_START_LINE}:I${RESULT_END_LINE}`).clearContent();

  // Uncheck columns
  SPREADSHEET.getRange(`D${RESULT_START_LINE}:D${RESULT_END_LINE}`).uncheck();
  SPREADSHEET.getRange(`J${RESULT_START_LINE}:J${RESULT_END_LINE}`).uncheck();
}

function clean(){
  // clean SEARCH CELLS
  SPREADSHEET.getRange(`${SEARCHED_ITEM_CELL}:${SEARCHED_QTY_CELL}`).clearContent();

  // Clean Debug console
  SPREADSHEET.getRange(`${LOG_CELL}`).clearContent();

  // Clean results
  _cleanResult();
}

function _raiseWarningMessage(message){
  _clr();
  _log(`[WARNING] ${message}`);
  _writeLog();
}



function setBuildOrder(item, buildOrder){
    item =  [...item, 0];
    return item;
}


function findClosestToTreeLeaves(craftableItemList, gatherableItemList){
  let  firstCrafts = {};
  //Les items qui sont construits à partir des gatherables sont les premiers par defaut
  for (const [key, value] of Object.entries(gatherableItemList)) {
    const usedAsIngredientFor = value["usedAsIngredientFor"];
    usedAsIngredientFor.forEach(craft=>{
      Object.keys(craftableItemList).includes(craft);
      firstCrafts[craft]=1;
    });

  }
  return firstCrafts;
}

function isUsedFor(item){
  const itemName = item[0][0];
  return CRAFT_STRATEGY_APPENDER[itemName]["usedAsIngredientFor"];
}

function valueLeafDistance(item, orderedItems, distance=2){
  // findParentItems that are part of 
  const foundItem = _findItem(item);
  const itemName = foundItem[0][0]
  const itemThatCanBeBuilt = isUsedFor(foundItem);

  // if the distance already has been calculated, it means that another item can craft the current one
  // in that case the distance is the biggest one
  if (!(itemName in orderedItems) || orderedItems[itemName]< distance){
    orderedItems[itemName]=distance;
  }

  itemThatCanBeBuilt.forEach(itm=>{
    valueLeafDistance(itm, orderedItems, distance+1);
  });
  return orderedItems;
}

function _buildOrder(craftableItemList, gatherableItemList){
  // 1. Find started leaves
  const orderedItems = findClosestToTreeLeaves(craftableItemList, gatherableItemList);

  // 2. iterate until the top of the tree while calculating distance from leaves
  for (const [key, value] of Object.entries(orderedItems)){
    valueLeafDistance(key, orderedItems, value);
  }; 

  // 3. sort it by buildOrder number
  const orderedArray = Object.entries(orderedItems).sort((a, b)=>a[1]-b[1]);

  return orderedArray;
}

function getQuantity(itemName){
  const qty = CRAFTED_ARRAY.filter(item=>item[0]===itemName)[0][1];
  //_log(qty);
  return qty;
}

/**
 * Main function retrieving the list of gathered materials
 */
function compute(){
  
  // Checks
  if (_getSearchItemName()+"".trim()==="" || _getSearchItemQty()+"".trim()===""){
    _raiseWarningMessage("Please select an item to craft and a quantity !");
    return;
  }
  if (_isGatherable(_getSearchItemName())){
    _raiseWarningMessage("You cannot craft this item !");
    return;
  }

  
  refreshItems();
  _cleanResult();
  _getMaterials();
  _log("GATHERABLES --------------");
  _log(GATHERABLE_APPENDER, true);
  _log(" ");
  _log(" ");
  _log("CRAFTABLES --------------");
  _log(CRAFT_STRATEGY_APPENDER, true);

  GATHERED_ARRAY= _objectToArray(GATHERABLE_APPENDER);;
  CRAFTED_ARRAY=_objectToArray(CRAFT_STRATEGY_APPENDER);
  
  const orderedCrafts = _buildOrder(CRAFT_STRATEGY_APPENDER, GATHERABLE_APPENDER);

  _log(" ");
  _log("BUILD ORDER --------------");
  _log(orderedCrafts ,true);

  const orderedWithQty = orderedCrafts.map(line=>[ line[1], line[0], getQuantity(line[0])]);

  //Display Gatherable mats & Craftable items
  _writeToCells(GATHERED_ARRAY, orderedWithQty );

  _writeLog();

}



