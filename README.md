# nms-materials

# NMS-Materials

## Introduction
NMS-Materials stands for No Man's Sky Material. Those scripts a meant to be used through Google SpreadSheet.
The purpose is to help you craft things by telling you what you'll need to craft step by step.
Here is the Google Spreadsheet : https://docs.google.com/spreadsheets/d/1ZiK2ElllE5Lh_UvGFKNpKt6INf-M3LL7s_HorkAHFXo/edit?usp=sharing



## Getting started

- Create your own copy of the spreadsheet [()](https://docs.google.com/spreadsheets/d/1ZiK2ElllE5Lh_UvGFKNpKt6INf-M3LL7s_HorkAHFXo/edit?usp=sharing)
- update the ```Code.js``` file and propose a merge request

## Conditions
The licence is MIT so you can do what ever you want, but it could be fair to mention this git as source of your own project.